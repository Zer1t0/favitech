#!/usr/bin/env python3

from favitech.__main__ import main

if __name__ == '__main__':
    exit(main())
