# favitech

Detect web technologies based on favicons. Based on [FavFreak](https://github.com/devanshbatham/FavFreak).

## Usage

```
$ subfinder -silent -d mydomain.com | httpx -silent | favitech
https://big-itsmapi.mydomain.com ASP.net 1772087922
http://azureconsumer.mydomain.com Windows Azure -2125083197
http://files.mydomain.com Windows Azure -2125083197
https://manage.prod.apac.ngps.mydomain.com React -2009722838
```

## Installation

```
pip install favitech
```
